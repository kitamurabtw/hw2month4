import Foundation

protocol delegate {
    
    func setRu() -> [Words]
    
    func setEn() -> [Words]
    
}

struct Words {
    let id: Int
    let lang: String
    let word: String
    let transcript: String
    let descript: String
}

class Storage: delegate {
    func setRu() -> [Words] {
        let words = ruWords
        return words
    }
    
    func setEn() -> [Words] {
        let words = enWords
        return words
    }

    
    var enWords: [Words] = [Words(id: 0, lang: "en", word: "stick", transcript: "stɪk",                          descript: "lol, its a stick"),
                            Words(id: 1, lang: "en", word: "apple", transcript: "æpəl", descript: "its a fruit"),
                            Words(id: 2, lang: "en", word: "chair", transcript: "tʃɛr", descript: "its box, and u can seat on this box"),
                            Words(id: 3, lang: "en", word: "glass", transcript: "glæs", descript: "its a little pool for drink")]
    var ruWords: [Words] = [Words(id: 0, lang: "ru", word: "палка", transcript: "па́лка",                           descript: "ну, палку можно использовать чтобы скрафтить кирку"),
                            Words(id: 1, lang: "ru", word: "яблоко", transcript: "я́блоко", descript: "ну часто в мультиках, это округлый фрукт красного цвета"),
                            Words(id: 2, lang: "ru", word: "стул", transcript: "сту́л", descript: "это объект на котором ты можешь более ли менее удобно сидеть"),
                            Words(id: 3, lang: "ru", word: "стакан", transcript: "ста́кан", descript: "это мини бассейн из которого ты можешь попить")]
}
