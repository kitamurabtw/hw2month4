import UIKit
import SnapKit

class WordViewController: UIViewController {
    
    var wordTextLabel = UILabel()
    var lineLabel = UILabel()
    var transcriptText = UILabel()
    var translateText = UILabel()
    
    var likedWords: [String] = []
    
    var word: String = ""
    var transcript: String = ""
    var descript: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layout()
        view.backgroundColor = .white
        
    }
    
    private func layout() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "chevron.backward"), style: .plain, target: self, action: #selector(goToMagazine))
        navigationItem.leftBarButtonItem?.tintColor = .black
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "heart"), style: .plain, target: self, action: #selector(addInLiked))
        navigationItem.rightBarButtonItem?.tintColor = .black
        
        view.addSubview(wordTextLabel)
        wordTextLabel.text = word
        wordTextLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        wordTextLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(109)
            make.left.equalToSuperview().inset(22)
            make.width.equalTo(124)
            make.height.equalTo(16)
        }
        
        view.addSubview(lineLabel)
        lineLabel.backgroundColor = UIColor(red: 0.764, green: 0.777, blue: 0.783, alpha: 1)
        lineLabel.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(22)
            make.top.equalToSuperview().inset(157)
            make.height.equalTo(1)
        }
        
        view.addSubview(transcriptText)
        transcriptText.text = transcript
        transcriptText.font = UIFont.systemFont(ofSize: 16)
        transcriptText.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(23)
            make.top.equalToSuperview().inset(178)
        }
        
        view.addSubview(translateText)
        translateText.text = descript
        translateText.numberOfLines = 15
        translateText.font = UIFont.systemFont(ofSize: 16)
        translateText.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(23)
            make.top.equalToSuperview().inset(200)
            make.right.equalToSuperview().inset(8)
        }
    }
    
    @objc func goToMagazine() {
        dismiss(animated: true, completion: nil)
    }
    var id = 0
    @objc func addInLiked() {
        if id == 0 {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "heart.fill"), style: .plain, target: self, action: #selector(addInLiked))
            navigationItem.rightBarButtonItem?.tintColor = .black
            id += 1
        } else if id == 1 {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "heart"), style: .plain, target: self, action: #selector(addInLiked))
            navigationItem.rightBarButtonItem?.tintColor = .black
            id -= 1
        }
    }
}
