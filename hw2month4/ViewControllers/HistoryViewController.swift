import UIKit

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var tabBarView = UILabel()
    
    var firstTabView = UILabel()
    var firstImageView = UIImageView()
    var firstBtn = UIButton()
    
    var secondTabView = UILabel()
    var secondImageView = UIImageView()
    var secondBtn = UIButton()
    
    var thirdTabView = UILabel()
    var thirdImageView = UIImageView()
    var thirdBtn = UIButton()
    
    var tableView = UITableView()
    
    var Delegate: delegate = Storage()
    var histWords: [String] = []
    var histTranscript: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        tabBarLayout()
        layout()
        tableView.register(CustomCell.self, forCellReuseIdentifier: CustomCell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func layout() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(16)
            make.top.equalToSuperview().inset(61)
            make.bottom.equalToSuperview().inset(161)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        histWords.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var word: [String] = []
        var transcript: [String] = []
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomCell.identifier, for: indexPath) as! CustomCell
        cell.wordText.text = histWords[indexPath.row]
        cell.transcriptText.text = histTranscript[indexPath.row]
        return cell
    }
    
    private func tabBarLayout() {
        view.addSubview(tabBarView)
        tabBarView.backgroundColor = UIColor(red: 0.851, green: 0.851, blue: 0.851, alpha: 1)
        tabBarView.layer.masksToBounds = true
        tabBarView.layer.cornerRadius = 20
        tabBarView.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(8)
            make.bottom.equalToSuperview().inset(14)
            make.right.equalToSuperview().inset(7)
            make.height.equalTo(77)
        }
        
        view.addSubview(firstTabView)
        firstTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        firstTabView.layer.masksToBounds = true
        firstTabView.layer.cornerRadius = 28
        firstTabView.snp.makeConstraints { make in
            make.top.equalTo(tabBarView).inset(11)
            make.left.equalTo(tabBarView).inset(15)
            make.width.equalTo(103)
            make.height.equalTo(55)
        }
        
        view.addSubview(firstImageView)
        firstImageView.image = UIImage(systemName: "magnifyingglass")
        firstImageView.tintColor = .black
        firstImageView.snp.makeConstraints { make in
            make.top.equalTo(firstTabView).inset(14)
            make.left.right.equalTo(firstTabView).inset(36)
            make.bottom.equalTo(firstTabView).inset(13)
        }
        
        view.addSubview(firstBtn)
        firstBtn.layer.cornerRadius = 28
        firstBtn.addTarget(self, action: #selector(goHome), for: .touchUpInside)
        firstBtn.snp.makeConstraints { make in
            make.top.equalTo(tabBarView).inset(11)
            make.left.equalTo(tabBarView).inset(15)
            make.width.equalTo(103)
            make.height.equalTo(55)
        }
        
        view.addSubview(secondTabView)
        view.addSubview(secondImageView)
        view.addSubview(secondBtn)
        
        secondTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        secondTabView.layer.masksToBounds = true
        secondTabView.layer.cornerRadius = 28
        secondImageView.image = UIImage(systemName: "heart")
        secondImageView.tintColor = .black
        secondBtn.layer.cornerRadius = 28
        secondBtn.addTarget(self, action: #selector(goLiked), for: .touchUpInside)
        secondTabView.snp.makeConstraints { make in
            make.top.bottom.equalTo(tabBarView).inset(11)
            make.center.equalTo(tabBarView)
            make.width.equalTo(103)
        }
        secondImageView.snp.makeConstraints { make in
            make.top.bottom.equalTo(secondTabView).inset(14)
            make.left.equalTo(secondTabView).inset(33)
            make.right.equalTo(secondTabView).inset(35)
        }
        secondBtn.snp.makeConstraints { make in
            make.center.equalTo(secondTabView)
            make.height.equalTo(55)
            make.width.equalTo(103)
        }
        
        view.addSubview(thirdTabView)
        view.addSubview(thirdImageView)
        view.addSubview(thirdBtn)
        
        thirdTabView.layer.masksToBounds = true
        thirdTabView.layer.cornerRadius = 28
        thirdTabView.backgroundColor = .white
        thirdImageView.image = UIImage(systemName: "gobackward")
        thirdImageView.tintColor = .black
        thirdBtn.addTarget(self, action: #selector(goHistory), for: .touchUpInside)
        thirdTabView.snp.makeConstraints { make in
            make.top.bottom.equalTo(tabBarView).inset(11)
            make.right.equalTo(tabBarView).inset(15)
            make.width.equalTo(103)
        }
        thirdImageView.snp.makeConstraints { make in
            make.top.bottom.equalTo(thirdTabView).inset(11)
            make.left.equalTo(thirdTabView).inset(35)
            make.right.equalTo(thirdTabView).inset(34)
        }
        thirdBtn.snp.makeConstraints { make in
            make.center.equalTo(thirdTabView)
            make.height.equalTo(55)
            make.width.equalTo(103)
        }
    }
    
    @objc func goHome() {
        firstTabView.backgroundColor = .white
        secondTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        thirdTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        
        let rootVC = ViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: false)
    }
    @objc func goLiked() {
        firstTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        secondTabView.backgroundColor = .white
        thirdTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        
        let rootVC = LikedViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: false)
    }
    @objc func goHistory() {
        firstTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        secondTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        thirdTabView.backgroundColor = .white
        
        let rootVC = HistoryViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: false)
    }
}
