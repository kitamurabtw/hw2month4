import UIKit
import SnapKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tabBarView = UILabel()
    
    var firstTabView = UILabel()
    var firstImageView = UIImageView()
    var firstBtn = UIButton()
    
    var secondTabView = UILabel()
    var secondImageView = UIImageView()
    var secondBtn = UIButton()
    
    var thirdTabView = UILabel()
    var thirdImageView = UIImageView()
    var thirdBtn = UIButton()
    
    var searchTextField = UITextField()
    var tableView = UITableView()
    var s: UITableViewDelegate?
    
    private lazy var setLang: UIButton = {
        let view = UIButton()
        return view
    }()
    
    let Delegate: delegate = Storage()
    var words: [Words] = []
    var histWords: [String] = []
    var histTranscript: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        words = Delegate.setEn()
        view.backgroundColor = .white
        tableView.register(CustomCell.self, forCellReuseIdentifier: CustomCell.identifier)
        tableView.separatorColor = .white
        tableView.delegate = self
        tableView.dataSource = self
        tabBarLayout()
        layout()
        setupLang()
        
        navigationController?.navigationBar.removeFromSuperview()
    }
    
    private func layout() {
        view.addSubview(searchTextField)
        searchTextField.backgroundColor = UIColor(red: 0.851, green: 0.851, blue: 0.851, alpha: 1)
        searchTextField.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        searchTextField.placeholder = "         SEARCH"
        searchTextField.layer.cornerRadius = 10
        searchTextField.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(8)
            make.top.equalToSuperview().inset(90)
            make.height.equalTo(57)
        }
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.top.equalTo(searchTextField).inset(99)
            make.left.equalToSuperview().inset(17)
            make.right.equalToSuperview().inset(16)
            make.bottom.equalTo(tabBarView).inset(99)
        }
    }
    
    let ruLabel = UILabel()
    let enLabel = UILabel()
    let lineLabel = UILabel()
    let backView = UIView()
    func setupLang() {
        view.addSubview(backView)
        backView.backgroundColor = UIColor(red: 0.851, green: 0.851, blue: 0.851, alpha: 1)
        backView.layer.cornerRadius = 10
        backView.snp.makeConstraints { make in
            make.bottom.equalTo(searchTextField).inset(68)
            make.right.equalToSuperview().inset(16)
            make.height.equalTo(31)
            make.width.equalTo(87)
        }
        backView.addSubview(enLabel)
        enLabel.text = "EN"
        enLabel.textColor = .white
        enLabel.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        enLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(7)
            make.right.equalToSuperview().inset(53)
            make.height.equalTo(16)
            make.width.equalTo(20)
        }
        backView.addSubview(ruLabel)
        ruLabel.text = "RU"
        ruLabel.font = UIFont.systemFont(ofSize: 13.5, weight: .bold)
        ruLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(7)
            make.right.equalToSuperview().inset(13)
            make.width.equalTo(20)
            make.height.equalTo(16)
        }
        backView.addSubview(lineLabel)
        lineLabel.backgroundColor = .white
        lineLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(3)
            make.left.equalToSuperview().inset(42)
            make.bottom.equalToSuperview().inset(4)
            make.width.equalTo(2)
        }
        backView.addSubview(setLang)
        setLang.addTarget(self, action: #selector(changeLang), for: .touchUpInside)
        setLang.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
        }
    }
    
    var id = 1
    @objc func changeLang() {
        //Если айди равно 0 значит стоит английский язык, если 1 то русский
        words = []
        if id == 0 {
            id = 1
            words = Delegate.setEn()
        } else {
            id = 0
            words = Delegate.setRu()
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        words.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomCell.identifier, for: indexPath) as! CustomCell
        cell.wordText.text = words[indexPath.row].word
        cell.transcriptText.text = words[indexPath.row].transcript
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rootVC = WordViewController()
        let word = words[indexPath.row].word
        let transcript = words[indexPath.row].transcript
        let descript = words[indexPath.row].descript
        histTranscript.append(words[indexPath.row].transcript)
        histWords.append(words[indexPath.row].word)
        rootVC.word = word
        rootVC.transcript = transcript
        rootVC.descript = descript
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: false)
    }
    
    private func tabBarLayout() {
        view.addSubview(tabBarView)
        tabBarView.backgroundColor = UIColor(red: 0.851, green: 0.851, blue: 0.851, alpha: 1)
        tabBarView.layer.masksToBounds = true
        tabBarView.layer.cornerRadius = 20
        tabBarView.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(8)
            make.bottom.equalToSuperview().inset(14)
            make.right.equalToSuperview().inset(7)
            make.height.equalTo(77)
        }
        
        view.addSubview(firstTabView)
        firstTabView.backgroundColor = .white
        firstTabView.layer.masksToBounds = true
        firstTabView.layer.cornerRadius = 28
        firstTabView.snp.makeConstraints { make in
            make.top.equalTo(tabBarView).inset(11)
            make.left.equalTo(tabBarView).inset(15)
            make.width.equalTo(103)
            make.height.equalTo(55)
        }
        
        view.addSubview(firstImageView)
        firstImageView.image = UIImage(systemName: "magnifyingglass")
        firstImageView.tintColor = .black
        firstImageView.snp.makeConstraints { make in
            make.top.equalTo(firstTabView).inset(14)
            make.left.right.equalTo(firstTabView).inset(36)
            make.bottom.equalTo(firstTabView).inset(13)
        }
        
        view.addSubview(firstBtn)
        firstBtn.layer.cornerRadius = 28
        firstBtn.addTarget(self, action: #selector(goHome), for: .touchUpInside)
        firstBtn.snp.makeConstraints { make in
            make.top.equalTo(tabBarView).inset(11)
            make.left.equalTo(tabBarView).inset(15)
            make.width.equalTo(103)
            make.height.equalTo(55)
        }
        
        view.addSubview(secondTabView)
        view.addSubview(secondImageView)
        view.addSubview(secondBtn)
        
        secondTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        secondTabView.layer.masksToBounds = true
        secondTabView.layer.cornerRadius = 28
        secondImageView.image = UIImage(systemName: "heart")
        secondImageView.tintColor = .black
        secondBtn.layer.cornerRadius = 28
        secondBtn.addTarget(self, action: #selector(goLiked), for: .touchUpInside)
        secondTabView.snp.makeConstraints { make in
            make.top.bottom.equalTo(tabBarView).inset(11)
            make.center.equalTo(tabBarView)
            make.width.equalTo(103)
        }
        secondImageView.snp.makeConstraints { make in
            make.top.bottom.equalTo(secondTabView).inset(14)
            make.left.equalTo(secondTabView).inset(33)
            make.right.equalTo(secondTabView).inset(35)
        }
        secondBtn.snp.makeConstraints { make in
            make.center.equalTo(secondTabView)
            make.height.equalTo(55)
            make.width.equalTo(103)
        }
        
        view.addSubview(thirdTabView)
        view.addSubview(thirdImageView)
        view.addSubview(thirdBtn)
        
        thirdTabView.layer.masksToBounds = true
        thirdTabView.layer.cornerRadius = 28
        thirdTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        thirdImageView.image = UIImage(systemName: "gobackward")
        thirdImageView.tintColor = .black
        thirdBtn.addTarget(self, action: #selector(goHistory), for: .touchUpInside)
        thirdTabView.snp.makeConstraints { make in
            make.top.bottom.equalTo(tabBarView).inset(11)
            make.right.equalTo(tabBarView).inset(15)
            make.width.equalTo(103)
        }
        thirdImageView.snp.makeConstraints { make in
            make.top.bottom.equalTo(thirdTabView).inset(11)
            make.left.equalTo(thirdTabView).inset(35)
            make.right.equalTo(thirdTabView).inset(34)
        }
        thirdBtn.snp.makeConstraints { make in
            make.center.equalTo(thirdTabView)
            make.height.equalTo(55)
            make.width.equalTo(103)
        }
    }
    
    @objc func goHome() {
        firstTabView.backgroundColor = .white
        secondTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        thirdTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        
        let rootVC = ViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: false)
    }
    @objc func goLiked() {
        firstTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        secondTabView.backgroundColor = .white
        thirdTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        
        let rootVC = LikedViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: false)
    }
    @objc func goHistory() {
        firstTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        secondTabView.backgroundColor = UIColor(red: 0.958, green: 0.906, blue: 0.906, alpha: 1)
        thirdTabView.backgroundColor = .white
        let histWords = histWords
        let histTranscript = histTranscript
        let rootVC = HistoryViewController()
        rootVC.histWords = histWords
        rootVC.histTranscript = histTranscript
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: false)
    }
}

