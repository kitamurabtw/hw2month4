//
//  CustomCellCol.swift
//  hw2month4
//
//  Created by Александр Калашников on 17/8/22.
//

import UIKit

class CustomCellCol: UICollectionViewCell {
    static let identifier = "CustomCellCol"
    
    var imageView = UIImageView()
    var wordText = UILabel()
    var translateText = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = UIColor(red: 0.851, green: 0.851, blue: 0.851, alpha: 1)
        contentView.layer.cornerRadius = 10
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    private func layout() {
        contentView.addSubview(imageView)
        imageView.image = UIImage(named: "space")
        imageView.layer.cornerRadius = 15
        imageView.layer.masksToBounds = true
        imageView.layer.shadowColor = .init(red: 0, green: 0, blue: 0, alpha: 0.25)
        imageView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(24.66)
            make.left.equalToSuperview().inset(88.29)
            make.right.equalToSuperview().inset(87.32)
            make.bottom.equalToSuperview().inset(109.6)
        }
        
        contentView.addSubview(wordText)
        wordText.text = "World"
        wordText.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        wordText.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(182.67)
            make.left.equalToSuperview().inset(62.09)
            make.right.equalToSuperview().inset(54.33)
            make.bottom.equalToSuperview().inset(73.98)
        }
        
        contentView.addSubview(translateText)
        translateText.text = "Transcript and translate"
        translateText.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        translateText.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(207.33)
            make.left.equalToSuperview().inset(62.09)
            make.bottom.equalToSuperview().inset(37.45)
            make.right.equalToSuperview().inset(54.33)
        }
    }
}
