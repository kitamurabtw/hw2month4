import UIKit
import SnapKit

class CustomCell: UITableViewCell {
    static let identifier = "CustomCell"
    
    var mainView = UIView()
    var wordText = UILabel()
    var transcriptText = UILabel()
    var icon = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.heightAnchor.constraint(equalToConstant: 101).isActive = true
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        contentView.addSubview(mainView)
        mainView.backgroundColor = UIColor(red: 0.957, green: 0.906, blue: 0.906, alpha: 1)
        mainView.layer.cornerRadius = 5
        mainView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(11.5)
            make.left.right.equalToSuperview().inset(0)
        }
        
        mainView.addSubview(wordText)
        wordText.text = "word"
        wordText.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        wordText.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(22)
            make.left.equalToSuperview().inset(18)
            make.bottom.equalToSuperview().inset(39)
            make.right.equalToSuperview().inset(198)
        }
        
        mainView.addSubview(transcriptText)
        transcriptText.text = "transcript"
        transcriptText.font = UIFont.systemFont(ofSize: 14)
        transcriptText.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(44)
            make.left.bottom.equalToSuperview().inset(18)
            make.right.equalToSuperview().inset(200)
        }
        
        mainView.addSubview(icon)
        icon.image = UIImage(systemName: "info.circle")
        icon.tintColor = UIColor(red: 0.248, green: 0.684, blue: 0.929, alpha: 1)
        icon.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(24)
            make.right.equalToSuperview().inset(13)
            make.height.width.equalTo(30)
        }
    }
}
